import express, { Request, Response } from 'express'

import applianceDB from './db'
import ApplianceService from './applianceService';
import appliances from './db';

// DI
const applianceService = new ApplianceService(applianceDB)


const router = express.Router()

router.get('/:id?', (req: Request, res: Response) => {
  if (req.params.id) {
    const appliance = applianceService.find(parseInt(req.params.id, 10))
    if (appliance == null) {
      return res.status(404).end();
    }
    return res.json({
      data: appliance,
    })
  }

  const appliance = applianceService.getAll()
  // todo convert case
  return res.json({
    data: appliance,
    meta: {
      page: 1,
      total: appliance.length,
      perPage: null
    }
  })
})

// todo auth
// todo validation
router.post('/', (req: Request, res: Response) => {
  const appliances = applianceService.create(req.body)

  return res.status(201).json({
    data: appliances
  })
})

// todo auth
router.delete('/:id', (req: Request, res: Response) => {
  applianceService.delete(parseInt(req.params.id, 10))

  return res.status(204).end()
})

// todo auth
// todo validation
router.put('/:id', (req: Request, res: Response) => {
  applianceService.update(parseInt(req.params.id, 10), req.body)
})


export default router
