import express, { Request, Response } from 'express'
import cors from 'cors'

import routes from './routes'
import ApplianceService from './applianceService';
import appliances from './db';

const app = express()

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// todo auth
app.delete('/system/', (req: Request, res: Response) => {
  // 202

})

app.use('/appliances', routes)

export default app
