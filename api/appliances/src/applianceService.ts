import ApplianceDto from './appliance.dto';

// todo all method should be async
export default class ApplianceService {
  constructor(
    private appliances: ApplianceDto[]
  ) { }

  public find(id: number): ApplianceDto | null {
    return this.appliances.find((appliance: ApplianceDto) => appliance.id === id) ?? null
  }

  public getAll(): ApplianceDto[] {
    return this.appliances
  }

  public create(newApplianceDto: ApplianceDto): ApplianceDto {
    let id = this.appliances.reduce((maxId, cur): number => {
      return maxId > cur.id ? maxId : cur.id
    }, 0) + 1
    let appliance = new ApplianceDto(id, newApplianceDto.name, new Date())
    this.appliances.push(appliance)

    return appliance
  }

  public update(id: number, newApplianceDto: ApplianceDto): ApplianceDto {
    let newAppliance = { ...newApplianceDto, id }
    this.appliances = this.appliances.map(
      (appliance: ApplianceDto) => appliance.id === id ? newAppliance : appliance
    )

    return newAppliance
  }

  public delete(id: number): void {
    this.appliances = this.appliances.filter(
      (appliances: ApplianceDto) => appliances.id !== id
    )
  }
}