import express, { Request, Response } from 'express'
import jwt from 'jsonwebtoken'
import cors from 'cors'
import cookieSession from 'cookie-session'

const app = express()

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use(
  cookieSession({
    signed: false,
    secure: false,
  })
)

// should be stored in db
const validUser = {
    id: 'abc123',
    email: 'test@vocovo.com',
  }

app.post('/auth/', (req: Request, res: Response) => {
  // todo it should be  submitted by user
  if (req.body.id !== validUser.id || req.body.email !== validUser.email) {
    return res.status(404).end()
  }

  // todo move token out
  // todo should use public / private key
  const userJwt = jwt.sign({
    id: req.body.id,
    email: req.body.email,
  }, 'Jm27CvRnQjxdYsD.Wvuq')

  req.session = {
    jwt: userJwt,
  }

  // todo set header 
  /**
   * Content-Type: application/json;charset=UTF-8
   * Cache-Control: no-store
   * Pragma: no-cache
   */
  res.json({
    access_token: userJwt,
    token_type: "JWT",
    expires_in: null,
  })
})

app.get('/auth/ping', (req, res) => {
  return res.json({ message: 'auth pong' })
})

export default app
