import { Form, Button } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { Navigate } from "react-router-dom";
import { RootState } from '../../store';
import { updateCredential, loginRequest } from './auth.slice';


export default function Login() {
  const loginFailed = useSelector((state: RootState) => state.auth.loginFailed)
  const credential = useSelector((state: RootState) => state.auth.credential)
  const isLogin = useSelector((state: RootState) => state.auth.token) !== ""
  const dispatch = useDispatch()
  const submit = function (e: React.FormEvent<HTMLInputElement>) {
    e.preventDefault()
    dispatch(loginRequest(credential))
  }

  if (isLogin) {
    return <Navigate to="/" />
  }

  return <Form>
    <Form.Group controlId="login-id">
      <Form.Label>Id</Form.Label>
      <Form.Control
        type="text"
        placeholder="Enter your id"
        value={credential.id}
        onChange={(e) => dispatch(updateCredential({ id: e.target.value }))}
      />
    </Form.Group>
    <Form.Group controlId="login-email">
      <Form.Label>Email</Form.Label>
      <Form.Control
        type="email"
        placeholder="Enter your email"
        value={credential.email}
        onChange={(e) => dispatch(updateCredential({ email: e.target.value }))}
      />
    </Form.Group>
    <Button
      variant="primary"
      type="submit"
      onClick={(e) => submit(e)}
    >Submit</Button>
    {loginFailed ? <p className="text-danger">Login fail, please try again</p> : null}
  </Form>
}