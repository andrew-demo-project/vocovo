export default class CredentialDto {
  constructor(
    public readonly email: string,
    public readonly id: string
  ) { }
}
