import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import { Axios } from "axios";
import { parseResponse } from '../../helpers';
import CredentialDto from "./credential.dto";

const initialState = {
  token: '',
  id: '',
  email: '',
  loginFailed: false,
  credential: {
    id: '',
    email: '',
  }
}

export const AuthSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    updateCredential: (state, action) => {
      state.credential = { ...state.credential, ...action.payload }
    },
    login: (state, action) => {
      state.token = action.payload.access_token
      state.id = state.credential.id
      state.email = state.credential.email
      state.loginFailed = false
    },
    failedLogin: (state) => {
      state.loginFailed = true
    },
    logout: (state) => {
      state.token = initialState.token
      state.id = initialState.id
      state.email = initialState.email
      state.loginFailed = initialState.loginFailed
      state.credential.id = initialState.credential.id
      state.credential.email = initialState.credential.email
    },
  }
})

const axios = new Axios({
  baseURL: "http://localhost:8080/auth",
  headers: {
    "content-type": "application/json",
  },
})

export const loginRequest = createAsyncThunk(
  "auth/login",
  async (data: CredentialDto, thunkApi) => {
    try {
      const response = await axios.post<{ access_token: string }>("/", JSON.stringify(data))
      const token = parseResponse(response.data).access_token;

      thunkApi.dispatch(AuthSlice.actions.login(token));
      console.log('login done');

    } catch (e: any) {
      console.log(e);

      thunkApi.dispatch(AuthSlice.actions.failedLogin());
    }
  }
)

export const { updateCredential, login, logout } = AuthSlice.actions

export default AuthSlice.reducer