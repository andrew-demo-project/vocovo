import Header from './header';
import ApplianceList from '../appliance/applianceList';
export default function AppliancePage() {
  return <div>
    <Header />
    <ApplianceList />
  </div>
}