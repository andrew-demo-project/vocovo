import { Link } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import { Button } from "react-bootstrap";
import { RootState } from '../../store';
import { logout } from '../auth/auth.slice'

export default function Header() {
  const isLogin = useSelector((state: RootState) => state.auth.token) !== ""
  const email = useSelector((state: RootState) => state.auth.email)
  const dispatch = useDispatch()

  const handleLogout = (e: React.FormEvent<HTMLButtonElement>) => {
    e.preventDefault()

    dispatch(logout())

  }

  const loginOrLogoutButton = () => {
    return isLogin ?
      <Button className="btn-warning" onClick={handleLogout}>Logout {email}</Button> :
      <Link to="/login" className="btn btn-primary ms-auto">Login in</Link>
  }


  return <header className="container-fluid">
    <h1 className="d-flex">Appliances</h1>
    {/* show back on login page */}
    {/* if not login */}
    {loginOrLogoutButton()}

    {/* if login, show name and logout */}
  </header>
}