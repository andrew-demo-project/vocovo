import Login from '../auth/loginForm';
import Header from './header';
export default function LoginPage() {
  return <div>
    <Header />
    <div className="container-fluid ">
      <Login />
    </div>
  </div>
}