import { useSelector } from 'react-redux';
import { RootState } from '../../store';
import ApplianceDto from './appliance.dto';
import ApplianceCreateForm from './applianceCreateForm';
export default function ApplianceList() {
  const appliances = useSelector((state: RootState) => state.appliances.appliances)
  const isLogin = useSelector((state: RootState) => state.auth.token) !== ""

  const appliancesList = appliances.map((appliance: ApplianceDto) => (
    <li key={appliance.id}>
      {appliance.id} {appliance.name} x
    </li>
  ))
  return <div className='container-fluid'>
    <div className="row">
      <ul className='col-8'>
      {appliancesList}
    </ul>
      <div className='col-4'>
        {isLogin ? <ApplianceCreateForm /> : null}
      </div>
    </div>
  </div>
}