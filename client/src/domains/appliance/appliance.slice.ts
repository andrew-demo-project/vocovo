
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { Axios } from 'axios';
import { parseResponse } from '../../helpers';
import ApplianceDto from './appliance.dto';

export const ApplianceSlice = createSlice({
  name: "appliance",
  initialState: {
    token: '',
    appliances: [] as ApplianceDto[],
    newAppliance: {
      name: '',
    } as ApplianceDto,
  },
  reducers: {
    list: (state, action) => {
      console.log(action.payload)
      state.appliances = action.payload
    },
    create: (state, action) => {
      state.appliances = state.appliances.concat([action.payload]) as ApplianceDto[]
      state.newAppliance.name = ''
    },
    updateNewAppliance: (state, action) => {
      state.newAppliance = { ...state.newAppliance, ...action.payload }
    },
    remove: (state) => {
    },
    update: (state, action) => {
    },
  }
})

const axios = new Axios({
  baseURL: "http://localhost:8080/appliances/",
  headers: {
    "Content-Type": "application/json",
  },
})

export const ApplianceFetchRequest = createAsyncThunk(
  "appliance/list",
  async (id: string, thunkApi) => {
    console.log("appliance/list")
    const response = await axios.get('/')
    thunkApi.dispatch(ApplianceSlice.actions.list(parseResponse(response.data).data))
  }
)

export const ApplianceCreateRequest = createAsyncThunk(
  "appliance/create",
  async (data: ApplianceDto, thunkApi) => {
    console.log("appliance/create")
    const response = await axios.post('/', JSON.stringify(data))
    console.log(response.data)

    thunkApi.dispatch(ApplianceSlice.actions.create(parseResponse(response.data).data))
  }
)

export const ApplianceUpdateRequest = createAsyncThunk(
  "appliance/update",
  async (data: any, thunkApi) => {
    console.log("appliance/update")
    const response = await axios.put('/', {})
    thunkApi.dispatch(ApplianceSlice.actions.update(parseResponse(response.data)))
  }
)

export const ApplianceDeleteRequest = createAsyncThunk(
  "appliance/delete",
  async (data: any, thunkApi) => {
    console.log("appliance/delete")
    await axios.delete('/', {})
    thunkApi.dispatch(ApplianceSlice.actions.remove())
  }
)

export const { list, create, remove, update, updateNewAppliance } = ApplianceSlice.actions

export default ApplianceSlice.reducer