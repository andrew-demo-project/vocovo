import { Form, Button } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../store';
import { create, updateNewAppliance, ApplianceCreateRequest } from './appliance.slice';

export default function ApplianceCreateForm() {
  const newAppliance = useSelector((state: RootState) => state.appliances.newAppliance)
  const dispatch = useDispatch()
  const submit = (e: React.FormEvent<HTMLButtonElement>) => {
    e.preventDefault()
    dispatch(ApplianceCreateRequest(newAppliance))
  }

  return <Form>
    <h3 className='h3'>Create new Appliance</h3>
    <Form.Group controlId="appliance-name">
      <Form.Label>Name</Form.Label>
      <Form.Control
        type="text"
        placeholder="Enter the appliance name"
        value={newAppliance.name}
        onChange={(e) => dispatch(updateNewAppliance({ name: e.target.value }))}
      />
    </Form.Group>
    <Button
      variant="primary"
      type="submit"
      onClick={submit}
    >Submit</Button>
  </Form>
}