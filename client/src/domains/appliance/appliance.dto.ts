export default class ApplianceDto {
  constructor(
    public readonly id: string,
    public readonly name: string
  ) { }
}
