import { configureStore } from '@reduxjs/toolkit'
import AuthSlice from './domains/auth/auth.slice';
import applianceSlice from './domains/appliance/appliance.slice';

export const store = configureStore({
  reducer: {
    auth: AuthSlice,
    appliances: applianceSlice,
  }
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch