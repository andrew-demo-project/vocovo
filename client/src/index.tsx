import React from 'react';
import ReactDOM from 'react-dom';
import { Routes, BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux'

import './index.scss';
import App from './App';
import reportWebVitals from './reportWebVitals';
import LoginPage from './domains/pages/login.page';
import AppliancePage from './domains/pages/appliance.page';
import { store } from './store';
import { ApplianceFetchRequest } from './domains/appliance/appliance.slice';

store.dispatch(ApplianceFetchRequest(''))

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
    <Router>
      <Routes>
        <Route path="/" element={<App />}>
          <Route path="/" element={<AppliancePage />} />
          <Route path="login" element={<LoginPage />} />
        </Route>
      </Routes>
    </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals(console.log);
